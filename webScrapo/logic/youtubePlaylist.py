from bs4 import BeautifulSoup
import requests
import re

class YPlaylist(object):

    def __init__(self,user_name, playlist_name):
        self.user_name = user_name
        self.playlist_name = playlist_name
        self.playlist_link = None

    def __find_playlist(self):
        user_name = self.user_name
        playlist_name = self.playlist_name

        html_link = "https://www.youtube.com/user/"+user_name+"/playlists"
        header = {"User-Agent": 'maaamu/5.0 (Windows NT 3.3; WffW64) Chome/69.0.3071.115 Safari/127.36'}
        request = requests.get(html_link)
        soup = BeautifulSoup(request.text, 'html.parser')
        if soup.find('div', {"class": "channel-empty-message"}):
            html_link = "https://www.youtube.com/channel/" + user_name + "/playlists"
            request = requests.get(html_link)
            soup = BeautifulSoup(request.text, 'html.parser')
        # print(html_link)
        # print(soup)

        playlist_link = soup.find('a',{"title": playlist_name})
        # print (soup)

        if (playlist_link):
            playlist_link= "https://www.youtube.com"+\
                           playlist_link.attrs['href']
            self.playlist_link = playlist_link
            return True

        else:
            print("location 2: error : playlist link")
            self.playlist_link = None
            return None


    def __explore_playlist(self):

        playlist_link = self.playlist_link

        request = requests.get(playlist_link)
        soup = BeautifulSoup(request.text, 'html.parser')

        playlist_data={}
        video_names={}
        video_links = soup.find_all('a',{"class": "pl-video-title-link"})

        if not video_links:
            print("location 3: error: No video links")
            return

        p_video_id0 = re.compile('watch\?v=[a-zA-Z0-9-_]+')
        p_video_id = re.compile('[a-zA-Z0-9-_]+$')
        p_index = re.compile('index=[0-9]+')
        p_index_id = re.compile('[0-9]+')

        for video_link in video_links:
            video_name = str(video_link.get_text())
            video_link = str(video_link.attrs['href'])
            cc = len(video_name) -5
            video_name = video_name[slice(7,cc)]
            video_index = p_index.search(video_link).group()
            video_index = int(p_index_id.search(video_index).group())-1

            video_link = p_video_id0.search(video_link).group()
            video_link = p_video_id.search(video_link).group()
            playlist_data[video_index] = video_link
            video_names[video_index] = video_name
        return {"playlist_data": playlist_data, "video_names": video_names}


    def extract(self):
        if self.__find_playlist():
            playlist_data = self.__explore_playlist()
            if (playlist_data):
                output_data = playlist_data

                print("Success")
                return output_data

            else:
                output_data = { 'playlist_data': "Empty Play list",
                                'video_names': "Empty Play list"
                               }

                print("error getting playlist:")
                return output_data

        else :
            output_data = { 'playlist_data': "No such playlist exists!",
                            'video_names': "No such playlist exists!"
                           }
            return output_data
