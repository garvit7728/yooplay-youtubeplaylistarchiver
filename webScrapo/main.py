from logic.youtubePlaylist import YPlaylist
import json
from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/api", methods=["POST"])
def hello():
    # print(request.json)
    req = request.json
    username = req['username']
    playlistname = req['playlistname']
    return json.dumps(YPlaylist(username, playlistname).extract())