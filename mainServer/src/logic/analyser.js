const ytdl = require('ytdl-core')
const EventEmitter = require('events')

class Analyser extends EventEmitter {

  constructor(playlist_data) {
    super()
    //this.limit = limit
    this.pldata = playlist_data
    //this.len_playlist = Object.keys(this.playlistData['playlist']).length
    //this.len_local = localPlaylistData['totalIndex']
    //this.indexPointer = limit
  }

  // playlistToConvert(callback) {

  //   //console.log(this)

  //   if (this.len_local == this.len_playlist) {
  //     console.log("Playlist is upto date")
  //     this.emit('uptodate')
  //   }

  //   if (this.len_local + this.limit > this.len_playlist) {
  //     this.limit = this.len_playlist

  //     for (let i = this.len_local + 1; i <= this.len_playlist; i++) {
  //       this.addAnalyser(i, (index, info) => {
  //         this.songMetaData(info, (dlLink, metaData) => {
  //           callback(index, dlLink, metaData)
  //         })
  //       })
  //     }

  //   } else {
  //     this.indexPointer = this.len_local + this.limit + 1
  //     for (let i = this.len_local + 1; i <= this.len_local + this.limit; i++) {
  //       //console.log(this.playlistData['playlist'][i])
  //       this.addAnalyser(i, (index, info) => {
  //         this.songMetaData(info, (dlLink, metaData) => {
  //           callback(index, dlLink, metaData)
  //         })
  //       })
  //     }
  //   }

  //   this.on('doneYTDL', () => {
  //     //This is the part in which parallel processes are added under limit
  //     if (this.indexPointer == this.len_playlist) {
  //       this.emit('100YTDL')
  //     }
  //     if (this.limit != this.len_playlist && this.indexPointer <= this.len_playlist) {
  //       this.addAnalyser(this.indexPointer, (index, info) => {
  //         this.songMetaData(info, (dlLink, metaData) => {
  //           callback(index, dlLink, metaData)
  //         })
  //       })
  //       this.indexPointer += 1
  //     }
  //   })
  // }

  analyseOne(index) {
    var pldata = this.pldata
    let songMetaData = this.songMetaData
    var promise = new Promise(function (resolve, reject) {
        // console.log(this.playlistData['playlist_data'][index])
        ytdl.getInfo('https://www.youtube.com/watch?v=' + pldata[index]).then((info) => {
          songMetaData(info, (dlLink, metaData) => {
            metaData['dlLink'] = dlLink
            resolve(metaData)
          })
        }).catch(reject)
    })
    return promise
  }

  addAnalyser(index, callback) {
    try {
      ytdl.getInfo('https://www.youtube.com/watch?v=' + this.playlistData['playlist'][index]).then((info) => {
        callback(index, info)
        this.emit('doneYTDL')
      })
    }
    catch (err) {
      console.log("Location addAnalyser: " + err)
    }
  }

  songMetaData(info, callback) {
    let songName = null
    let artist = null
    let album = null

    let title = info['title']
    let media = info['media']

    if (media) {
      if (media['song']) songName = media['song']
      if (media['artist']) artist = media['artist']
      if (media['album']) album = media['album']
    }

    if (songName == null) {
      songName = title
    }
    if (artist == null) {
      artist = "Unknown artist"
    }
    if (album == null) {
      album = "Unknown album"
    }
    //console.log(`Song-name: ${songName}\nArtist: ${artist}\nAblum: ${album}\n\n`)
    let metaData = {
      'songName': songName.replace(/[/\:*?"<>|]/g, "").slice(0, 50),
      'artist': artist,
      'album': album
    }

    formatAnalyser(info['formats'], (link) => {
      callback(link, metaData)
    })
  }
}

// Gives link as a callback of higest quality audio file
var formatAnalyser = (formats, callback) => {
  let audioBitrate = 10
  let audioEncodingList = ['opus', 'aac', 'vorbis']
  let audioEncoding = 2
  let link = ""
  formats.forEach((dlLink) => {
    //console.log(dlLink['audioEncoding'])
    //console.log(dlLink['audioBitrate'])
    if (dlLink['encoding'] == null &&
      dlLink['audioBitrate'] >= audioBitrate) {
      audioBitrate = dlLink['audioBitrate']
      if (audioEncodingList
        .indexOf(dlLink['audioEncoding']) <= audioEncoding) {
        link = dlLink['url']
        audioEncoding = audioEncodingList.indexOf(dlLink['audioEncoding'])
        // console.log("audioBitrate:"+audioBitrate+
        //  " :encoder: "+audioEncodingList[audioEncoding])
      }
    }
  })
  callback(link)
}

module.exports = { Analyser }
