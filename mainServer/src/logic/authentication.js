const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))

module.exports = (password, callback) => {
  // Method to hash a password
  const SALT_FACTOR = 8
  bcrypt
  .genSalt(SALT_FACTOR)
  .then(salt => bcrypt.hashAsync(password, salt, null))
  .then(hash => {
    callback(hash)
  })
}
