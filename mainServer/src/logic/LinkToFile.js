const { spawn } = require('child_process')
const path = require('path')
const EventEmitter = require('events')
const fs = require('fs')

class LinkToFileConverter extends EventEmitter {

  constructor() {
    super()
    //this.limit = limit
    //this.count = 0
    //this.backlog = []
    //this.len_local_out = 0
    //this.len_playlist = Object.keys(playlistData['playlist']).length
    //this.len_local = localPlaylistData['totalIndex']

    // this.on('processadded',()=>{
    //   this.addExitListener()
    // })
    // this.on('runprogress',()=>{
    //   this.getProgress()
    //   if(this.len_local_out==(this.len_playlist-this.len_local))
    //     this.emit('converted')
    // })
  }

  // convert() {
  //   if (this.backlog != null && this.backlog.length != 0) {
  //     this.backlog.forEach((item) => {

  //       let dlLink = item['dl']
  //       let metaData = item['meta']

  //       if (item['status'] == 'raw' && this.count < this.limit) {

  //         item['status'] = 'converting'
  //         item['exitlistener'] = false

  //         var fileName = metaData['songName']
  //         var songName = metaData['songName']
  //         var artist = metaData['artist']
  //         var album = metaData['album']

  //         var arr = ['-i', dlLink, '-c', 'opus',
  //           '-strict', '-2',
  //           '-metadata', `Title=${songName}`,
  //           '-metadata', `Artist=${artist}`,
  //           '-metadata', `Album=${album}`,
  //           fileName + '.ogg', '-y']
  //         // var arr = ['-i',dlLink,'-c','mp3',
  //         //                 '-metadata',`Title=${songName}`,
  //         //                 '-metadata',`Artist=${artist}`,
  //         //                 '-metadata',`Album=${album}`,
  //         //                 fileName+'.mp3','-y']

  //         var options = { cwd: path.join(__dirname, '../../downloads2') }
  //         this.count++
  //         item['process'] = spawn('ffmpeg', arr, options)
  //         //console.log('converting: '+fileName+'.ogg')
  //         this.emit('processadded')
  //       }

  //     })
  //   }
  // }

  convertOne (metaData, callback) {
    var fileName = metaData['songName']
    var songName = metaData['songName']
    var artist = metaData['artist']
    var album = metaData['album']
    var dlLink = metaData['dlLink']
    var arropus = ['-i', dlLink, '-c', 'opus',
      '-strict', '-2',
      '-metadata', `Title=${songName}`,
      '-metadata', `Artist=${artist}`,
      '-metadata', `Album=${album}`,
      fileName + '.ogg', '-y']
    var arrOriginal = ['-i', dlLink,
      '-metadata', `Title=${songName}`,
      '-metadata', `Artist=${artist}`,
      '-metadata', `Album=${album}`,
      fileName + '.ogg', '-y']
    var arrmp3 = ['-i', dlLink, '-c', 'mp3',
      '-metadata', `Title=${songName}`,
      '-metadata', `Artist=${artist}`,
      '-metadata', `Album=${album}`,
      fileName + '.mp3', '-y']
    let filePathogg = path.join(__dirname, '../../downloads', metaData['playlistid'],
      metaData['codec'], fileName + '.ogg')
    let filePathmp3 = path.join(__dirname, '../../downloads', metaData['playlistid'],
      metaData['codec'], fileName + '.mp3')

    var arr = []
    var filePath = ''

    if (metaData['codec'] === 'mp3') {
      arr = arrmp3
      filePath = filePathmp3
    }

    if (metaData['codec'] === 'ogg') {
      arr = arropus
      filePath = filePathogg
    }

    let dpath = path.join(__dirname, '../../downloads', metaData['playlistid'],
      metaData['codec'])

    if (fs.existsSync(filePath)) {
      return callback(filePath)
    }

    const child0 = spawn('mkdir', ['-p', dpath])

    child0.on('exit', (code) => {
      if (code === 0) {
        const child = spawn('ffmpeg', arr, { cwd: dpath })
        child.on('exit', (code) => {
          if (code === 0) {
            callback(filePath)
          } else if (code === null) {
            const child_2 = spawn('ffmpeg', arrOriginal, { cwd: dpath })
            child_2.on('exit', (code) => {
              if (code === 0) {
                callback(filePath)
              }
            })
          }
        })
      }
    })
  }

  // addExitListener(){
  //   this.backlog.forEach((spawn)=> {
  //     if(spawn['status']=='converting' && spawn['exitlistener'] ==false){
  //     spawn['process'].on('exit', (code,signal)=> {
  //       if(code == 0){
  //         spawn['status']='converted'
  //         //console.log(spawn['id']+': Converted: ' +spawn['meta']['songName']+'.ogg')
  //       }else{
  //         spawn['status']='errored'
  //       }
  //       this.count --
  //       this.emit('runprogress')
  //     })
  //     this.emit('runprogress')
  //     spawn['exitlistener'] = true
  //   }
  //  })
  // }

  //   getProgress(){
  //     this.len_local_out=0
  //     this.backlog.forEach((item)=>{
  //         if(item['status']=='converting'){
  //           //console.log(`CONVERTING: ${item['id']} ${item['meta']['songName']}`)

  //         }else if(item['status']=='converted'){
  //           //console.log(`CONVERTED: ${item['id']} ${item['meta']['songName']}`)
  //           this.len_local_out++
  //         }else if(item['status']=='errored'){
  //           //console.log(`ERRORED: ${item['id']} ${item['meta']['songName']}`)
  //           this.len_local_out++
  //         }else if(item['status']=='raw'){
  //           //console.log(`RAW: ${item['id']} ${item['meta']['songName']}`)
  //         }
  //     })

  //     this.emit('doneprogress')

  // }

  //   add(index,dlLink,metaData){
  //     this.backlog.push({ 'id':index, 'dl': dlLink, 'meta' : metaData, 'status': 'raw'})
  //   }
}

module.exports = { LinkToFileConverter }
