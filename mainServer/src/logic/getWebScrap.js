const http = require('http')

module.exports = (username, playlistname) => {
  var promise = new Promise(function (resolve, reject) {
    var options = {
      hostname: '0.0.0.0',
      port: 8090,
      path: `/api`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const postData = JSON.stringify({
      'username': username,
      'playlistname': playlistname
    })
    // console.log(postData)
    let buf = ''
    var parsedData = null
    const reqplay = http.request(options, (resp) => {
      resp.on('data', (chunk) => {
        buf += chunk
      })
      resp.on('end', () => {
        try {
          parsedData = JSON.parse(buf)

          if (parsedData['playlist_data'] !== 'No such playlist exists!') {
            resolve(parsedData)
          } else {
            reject({ error: 'Either Username or playlist does not exists' })
          }
        } catch (e) {
          console.error("error retrieving info from webscrapo")
          reject(e)
        }
      })
    })
    console.log(typeof (postData))
    reqplay.on('error', reject)
    reqplay.write(postData)
    reqplay.end()
  })

  return promise
}
