// Connect db for playlistdata data
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://localhost:27017/yooplay'
const collec = 'playlistdata'
module.exports = {
  async isPlaylistExists (playlist) {
    const client = await MongoClient.connect(url)
    var collection = client.db('yooplay').collection(collec)
    return collection.findOne({
      username: playlist.username,
      playlistname: playlist.playlistname
    })
  },
  async addPlaylist (playlist) {
    var seq = await getNextSequence('playlistid')
    seq = seq['value']['seq']
    const client = await MongoClient.connect(url)
    var collection = client.db('yooplay').collection(collec)
    collection.insertOne({
      playlistid: `pl${seq}`,
      username: playlist.username,
      playlistname: playlist.playlistname,
      playlist_data: playlist.playlist_data,
      video_names: playlist.video_names 
    })
    return `pl${seq}`
  },
  async getPlaylistData (playlist) {
    const client = await MongoClient.connect(url)
    var collection = client.db('yooplay').collection(collec)
    return collection.findOne({
      playlistid: playlist.playlistid
    })
  },
  async addFilePath (playlist) {
    const client = await MongoClient.connect(url)
    var collection = client.db('yooplay').collection(collec)
    return collection.updateOne({
      playlistid: playlist.playlistid
    }, {
      $set: {
        [playlist.codec + '.indexfilepath.' + playlist.index]: playlist.filepath,
        [playlist.codec + '.filestatus.' + playlist.index]: playlist.filestatus
      }
    })
  },
  async getVideoNamesStatus (playlistid) {
    const client = await MongoClient.connect(url)
    var collection = client.db('yooplay').collection(collec)
    return collection.findOne({
      playlistid: playlistid
    })
  },
  async updatePlaylist (playlist) {
    const client = await MongoClient.connect(url)
    var collection = client.db('yooplay').collection(collec)
    return collection.findOneAndUpdate({
      playlistid: playlist.playlistid
    }, {
      $set: {
        playlist_data: playlist.playlist_data,
        video_names: playlist.video_names,
        mp3: {},
        ogg: {}
      }
    })
  }
}
// insert one document in counters -- db.counters.insert({_id: "playlistid", sequence_value: 0})

async function getNextSequence (sname) {
  const client = await MongoClient.connect(url)
  var collection = client.db('yooplay').collection('counters')
  return collection.findOneAndUpdate({
    _id: sname
  },{
    $inc: {'seq': 1}
  },{
    projection: {seq: 1, _id: 0},
    returnNewDocument: true
  })
}
