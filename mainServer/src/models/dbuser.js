// Connect db for users data
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://localhost:27017'
const collec = 'authen'
module.exports = {
  async insertUser (user) {
    const client = await MongoClient.connect(url) 
    var collection = client.db('yooplay').collection(collec)
    var promise = new Promise(function (resolve, reject) {
      collection.count({ email: user.email }).then(count => {
        if (count === 0) {
          collection.insertOne({
            name: user.name,
            password: user.password,
            email: user.email,
            playlists: []
          })
          return resolve(true)
        } else {
          return resolve(false)
        }
      }).catch(reject)
    })

    return promise
  },
  async addPlaylist (user, playlist) {
    const client = await MongoClient.connect(url)
    var collection = client.db('yooplay').collection(collec)
        collection.updateOne({
          email: user.email
        }, {
          $addToSet: {
          'playlists': {
            'playlistid': playlist.playlistid,
            'plname': playlist.plname            
            }
          }
      })
  },
  async findUser (user) {
    const client = await MongoClient.connect(url)
    var collection = client.db('yooplay').collection('authen')
    return collection.findOne({ email: user.email })
  }
}
