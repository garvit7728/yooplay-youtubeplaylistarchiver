console.log('hello1')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const path = require('path')

const config = require('./config/config.js')
const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors({
  exposedHeaders: ['Content-Disposition']
}))

var DIST_DIR = path.join(__dirname, '../dist')
app.use(express.static(DIST_DIR))
require('./passport')
require('./routes/register')(app)
require('./routes/status')(app)
require('./routes/device')(app)
require('./routes/playlist')(app)
require('./routes/singleconvert')(app)
require('./routes/download')(app)
app.get('*', function (req, res) {
  res.sendFile(path.join(DIST_DIR, 'index.html'))
})

app.listen(config.port, () => {
  console.log(`Server started at ${config.port}`)
})
