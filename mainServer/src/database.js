let mongoose = require('mongoose')

class Database {
  constructor () {
    this._connect()
  }

  _connect () {
    mongoose.connect('mongodb://localhost/yooplay')
      .then(() => {
        console.log('Database connection successful')
      })
      .catch(err => {
        if (err) {
          console.log('Database connection error')
        }
      })
  }
}

module.exports = new Database()
