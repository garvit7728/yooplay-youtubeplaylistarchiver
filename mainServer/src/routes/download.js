const connectdbplaylist = require('../models/dbplaylist')
const isAuthenticated = require('../logic/isAuthenticated')
let token = 'ddfddefedfd'

module.exports = (app) => {
  app.get('/getdownloadlink', isAuthenticated, (req, res) => {
    let link = `/getdownloadfile?index=${req.query.index}&playlistid=${req.query.playlistid}&codec=${req.query.codec}&dtkn=${token}`
    res.status(200).send(link)
  })
  app.get('/getdownloadfile', (req, res) => {
    if (token !== req.query.dtkn) {
      return res.status(403).send('Dont have access to this link')
    }
    var playlist = {
      playlistid: req.query.playlistid,
      index: req.query.index,
      codec: ''
    }
    if (req.query.codec === '0') {
      playlist.codec = 'ogg'
    } else if (req.query.codec === '1') {
      playlist.codec = 'mp3'
    } else {
      return res.status(400).send({
        error: 'Codec not found'
      })
    }
    connectdbplaylist.getVideoNamesStatus(playlist.playlistid).then(doc => {
      if (doc === null) {
        return res.status(400).send({
          'error': 'No such playlsit exists'
        })
      }
      let finaldoc = {}

      if (doc.hasOwnProperty('ogg')) finaldoc['ogg'] = doc.ogg.indexfilepath
      if (doc.hasOwnProperty('mp3')) finaldoc['mp3'] = doc.mp3.indexfilepath

      if (finaldoc[playlist.codec].hasOwnProperty(playlist.index)) {
        res.download(finaldoc[playlist.codec][playlist.index])
      } else {
        return res.send({
          'error': 'file does not exists, Process it first.'
        })
      }
    })
      .catch(e => {
        console.log(`Error at location download.js 2a:${e.message}`)
        res.status(500).send({
          error: 'Error from server side'
        })
      })
  })
}
