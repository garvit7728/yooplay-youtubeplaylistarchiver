const ana = require('../logic/analyser')
const ltf = require('../logic/LinkToFile')
const connectdbplaylist = require('../models/dbplaylist')
module.exports = (app) => {
  app.post('/convert', (req, res) => {

    var playlist = {
      playlistid: req.body.playlistid,
      index: req.body.index,
      codec: ''
    }
    if (req.body.codec === '0') {
      playlist.codec = 'ogg'
    } else if (req.body.codec === '1') {
      playlist.codec = 'mp3'
    } else {
      return res.status(400).send({
        error: 'Codec not found'
      })
    }

    connectdbplaylist.getPlaylistData(playlist).then(getplaylist => {
      if (getplaylist === null) throw { message: 'No such playlist! Add a playlist first!' }

      playlist_data = getplaylist.playlist_data

      if (!playlist_data.hasOwnProperty(playlist.index)) throw { message: 'No such song!' }

      const analyse = new ana.Analyser(playlist_data)
      return analyse.analyseOne(playlist.index)
    }).then(metaData => {

      metaData['playlistid'] = playlist.playlistid
      metaData['codec'] = playlist.codec
      const converter = new ltf.LinkToFileConverter()
      converter.convertOne(metaData, (file_path) => {
        playlist['filepath'] = file_path
        playlist['filestatus'] = 'readytodownload'
        connectdbplaylist.addFilePath(playlist)
      })
      return res.status(200).send({ message: 'Processing' })
    }).catch(e => {
      res.status(200).send({
        error: e.message
      })
      console.log(`Error at location singleconvert.js 1a ${e.message}`)
    })
  })
}
