const connectdbplaylist = require('../models/dbplaylist')
const connectdbuser = require('../models/dbuser')
const getws = require('../logic/getWebScrap')
const isAuthenticated = require('../logic/isAuthenticated')

module.exports = (app) => {
  app.post('/addplaylist', isAuthenticated, (req, res) => {
    var user = {
      // name: req.body.name,
      // password: req.body.password,
      email: req.user.email
    }
    var playlist = {
      username: (req.body.username),
      playlistname: req.body.playlistname,
      playlist_data: null,
      video_names: null,
      plname: req.body.plname
    }
    connectdbplaylist.isPlaylistExists(playlist)
      .then(plobj => {
        if (plobj) {
          connectdbuser.addPlaylist(user, {
            playlistid: plobj.playlistid,
            plname: playlist.plname
          })
            .then(() => {
              // console.log(plobj)
              return res.status(200).send({
                playlistid: plobj.playlistid,
                plname: playlist.plname
              })
            }).catch((e) => {
              console.log('Error at location a1', e.message)
              return res.status(500).send({
                'error': 'Error on server side'
              })
            })
        } else {
          getws(playlist.username, playlist.playlistname)
            .then(parsedata => {
              // console.log('parsedata')
              playlist['playlist_data'] = parsedata['playlist_data']
              playlist['video_names'] = parsedata['video_names']
            }).then(() => {
              return connectdbplaylist.addPlaylist(playlist)
            }).then(plid => {
              playlist['playlistid'] = plid
              connectdbuser.addPlaylist(user, {
                playlistid: plid,
                plname: playlist.plname
              })
            })
            .then(() => {
              res.status(200).send({
                playlistid: playlist.playlistid,
                plname: playlist.plname
              })
            })
            .catch((e) => {
              if (e.hasOwnProperty('error') === true) {
                return res.status(202).send(e)
              }
              console.log(e.message)
              res.status(400).send({
                'error': 'error from server side'
              })
            })
        }
      })
  })
  app.post('/updateplaylist', isAuthenticated, (req, res) => {
    var playlist = {
      username: null,
      playlistname: null,
      playlist_data: null,
      video_names: null,
      playlistid: req.body.playlistid
    }
    connectdbplaylist.getVideoNamesStatus(playlist.playlistid)
      .then(plobj => {
        // console.log(plobj)
        if (plobj) {
          playlist['username'] = plobj.username
          playlist['playlistname'] = plobj.playlistname
          getws(plobj.username, plobj.playlistname)
            .then(parsedata => {
              playlist['playlist_data'] = parsedata['playlist_data']
              playlist['video_names'] = parsedata['video_names']
            })
            .then(() => {
              connectdbplaylist.updatePlaylist(playlist)
              return res.status(200).send({
                playlistid: playlist.playlistid,
                video_names: playlist.video_names,
                username: playlist.username,
                playlistname: playlist.playlistname
              })
            })
            .catch((e) => {
              if (e.hasOwnProperty('error') === true) {
                return res.status(202).send(e)
              }
              console.log(e.message)
              res.status(400).send({
                'error': 'error from server side'
              })
            })
        }
      })
  })
  app.post('/getvideonames', isAuthenticated, (req, res) => {
    var playlistid = req.body.playlistid
    let finaldoc = {
      username: '',
      playlistname: '',
      video_names: {},
      oggfilestatus: {},
      mp3filestatus: {}
    }
    connectdbplaylist.getVideoNamesStatus(playlistid).then(doc => {
      if (doc === null) return res.status(402).send(null)
      if (doc.hasOwnProperty('ogg') && doc.ogg.hasOwnProperty('filestatus')) finaldoc['oggfilestatus'] = doc.ogg.filestatus
      if (doc.hasOwnProperty('mp3') && doc.mp3.hasOwnProperty('filestatus')) finaldoc['mp3filestatus'] = doc.mp3.filestatus
      finaldoc['video_names'] = doc.video_names
      finaldoc['username'] = doc.username
      finaldoc['playlistname'] = doc.playlistname
      return res.status(203).send(finaldoc)
    })
      .catch(e => {
        console.log(`Error at location playlist.js 2a:${e.message}`)
        res.status(500).send({
          error: 'Error from server side'
        })
      })
  })
  // Which is normally going to be triggered when refreshing the file status
  app.post('/getfilestatus', (req, res) => {
    var playlistid = req.body.playlistid
    connectdbplaylist.getVideoNamesStatus(playlistid).then(doc => {
      if (doc === null) return res.status(400).send(null)
      let finaldoc = {}
      if (doc.hasOwnProperty('ogg') && doc.ogg.hasOwnProperty('filestatus')) finaldoc['oggfilestatus'] = doc.ogg.filestatus
      if (doc.hasOwnProperty('mp3') && doc.mp3.hasOwnProperty('filestatus')) finaldoc['mp3filestatus'] = doc.mp3.filestatus

      return res.status(200).send(finaldoc)
    })
      .catch(e => {
        console.log(`Error at location playlist.js 2a:${e.message}`)
        res.status(500).send({
          error: 'Error from server side'
        })
      })
  })
}
