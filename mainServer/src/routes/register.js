const validator = require('validator')
const jwt = require('jsonwebtoken')
const config = require('../config/config')
const bcrypt = require('bcrypt-nodejs')
const connectdb = require('../models/dbuser')

function jwtSignUser (user) {
  const ONE_WEEK =60*60*24*7
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_WEEK
  })
}

module.exports = (app) => {
  app.post('/register', (req, res) => {
    var user = {
      name: req.body.name,
      password: req.body.password,
      email: req.body.email.toLowerCase()
    }

    if (validator.isEmpty(user.name) || validator.isEmpty(user.password) || !validator.isEmail(user.email)) {
      return res.status(403).send({
        error: 'The Information entered is not sufficient'
      })
    }

      // TODO: Hashing password before storing it into the database
      user.password = bcrypt.hashSync(user.password)

      connectdb.insertUser(user).then(bool=> {
      if (bool) {
        return res.send({
          message: `Hello ${req.body.email}! You are registered! Have Fun!`
        })
      } else {
        return res.send({
          message: `Hello ${req.body.email}! is already registered! Use different one`
        })
      }
    }).catch(e => {
      return res.status(504).send({
        error: 'Server side error'
      })
    })
  }),
  app.post('/login', (req, res) => {
    var user = {
      password: req.body.password,
      email: req.body.email.toLowerCase()
    }

    if (validator.isEmpty(user.password) || !validator.isEmail(user.email)) {
      return res.status(403).send({
        error: 'The Information entered is not sufficient'
      })
    }

    connectdb.findUser(user).then(usrObj => {
      if (usrObj === null){
        return res.status(403).send({
          error: 'No Such user exists!'
        })
      }
      const isPassowrdValid = bcrypt.compareSync(user.password, usrObj.password) // user.password === usrObj.password
      if (!isPassowrdValid) {
        return res.status(403).send({
          error: ' The login information was incorrect'
        })
      }
      delete usrObj['_id']; delete usrObj['password']
      return res.status(200).send({
        message: 'You are logged in',
        user: usrObj,
        token: jwtSignUser(user)
      })
    }).catch(e => {
      console.error(`Error location 2a: ${e.message}`)
      res.status(405).send({
        error: 'Error on server side'
      })
    })
      // Send usrObj without password
  })
}
